package com.meru.CartService.exception;

public class CartNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CartNotFoundException(final String exceptionMessage)
	{
		super(exceptionMessage);
	}

}
