package com.meru.CartService.exception;

public class CartAlreadyExistException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3845037120087598230L;

	public CartAlreadyExistException(final String errorMessage)
	{
		super(errorMessage);
	}
}
