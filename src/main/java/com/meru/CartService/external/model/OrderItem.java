package com.meru.CartService.external.model;

import java.io.Serializable;


//import com.meru.OrderService.external.model.Product;

public class OrderItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8339898652973409086L;

	private long orderItemID;
	
	//@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	//private Product product;
	
	private long productID;
	
	private String productName;
	
	private int quantity;

	private double productPrice;
	
	public long getOrderItemID() {
		return orderItemID;
	}

	public void setOrderItemID(long orderItemID) {
		this.orderItemID = orderItemID;
	}
	
	public long getProductID() {
		return productID;
	}

	public void setProductID(long productID) {
		this.productID = productID;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	/*
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
*/
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
