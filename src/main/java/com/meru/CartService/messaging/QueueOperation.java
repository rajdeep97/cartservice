package com.meru.CartService.messaging;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import com.meru.CartService.service.CartService;

@Component
public class QueueOperation {
	
	@Autowired
	private CartService service;

	private final String CART_CREATE_QUEUE= "cart.CREATE";
	private final String CART_DELETE_QUEUE = "cart.DELETE";
	private final String ADD_PRODUCTS_TO_ORDER_REQUEST = "order.request.new.SET_PRODUCTS";
	private final String ADD_CART_TO_ORDER_REQUEST = "order.request.new.SET_CART";
	private final String ORDER_CONFIRMED = "order.CONFIRMED";
	
	@JmsListener(destination = CART_CREATE_QUEUE)
	public void createEmptyCartForNewCustomer(final Message msg)throws JMSException
	{
		if(msg instanceof MapMessage)
		{
			MapMessage mapmsg=(MapMessage)msg;
			if(mapmsg.propertyExists("customerID"))
			{
				long customerId=mapmsg.getLongProperty("customerID");
				this.service.createEmptyCartForCustomerId(customerId);
			}
		}
	}
	
	@JmsListener(destination = CART_DELETE_QUEUE)
	public void deleteCartForCustomer(final Message msg)throws JMSException
	{
		if(msg instanceof MapMessage)
		{
			MapMessage mapmsg=(MapMessage)msg;
			if(mapmsg.propertyExists("customerID"))
			{
				long customerId=mapmsg.getLongProperty("customerID");
				this.service.deleteCartForCustomerId(customerId);
			}
		}
	}
	
	@JmsListener(destination = ADD_CART_TO_ORDER_REQUEST)
	@SendTo(ADD_PRODUCTS_TO_ORDER_REQUEST)
	public String addCartToOrderRequest(String jsonOrderRequest)
	{
		return this.service.addCartToOrderRequest(jsonOrderRequest);
	}
	
	@JmsListener(destination = ORDER_CONFIRMED)
	public void resetCartAfterSuccessfulOrder(String jsonOrder)
	{
		this.service.resetCartAfterSuccessfulOrder(jsonOrder);
	}
}
