package com.meru.CartService.service.Impl;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.meru.CartService.exception.CartAlreadyExistException;
import com.meru.CartService.exception.CartNotFoundException;
import com.meru.CartService.exception.InvalidRequestException;
import com.meru.CartService.external.model.Order;
import com.meru.CartService.external.model.OrderRequest;
import com.meru.CartService.model.Cart;
import com.meru.CartService.model.CartItem;
import com.meru.CartService.repository.CartRepository;
import com.meru.CartService.service.CartService;

@Service
public class CartServiceImpl implements CartService {
	
	@Autowired
	private CartRepository repository;
	
	//@Autowired
	//private QueueOperation queueOperation;

	@Override
	public Cart createEmptyCartForCustomerId(long custId) {
		Optional<Cart> existingCart = this.repository.findCartByCustomerId(custId);
		if(existingCart.isPresent())
		{
			throw new CartAlreadyExistException("Cart already exist for CustomerId : "+custId);
		}
		
		Cart cart = new Cart();
		cart.setCustomerID(custId);
		cart.setShoppingCart(new ArrayList<CartItem>());
		return this.repository.save(cart);
	}

	@Override
	public Cart getCartForCustomerId(long custId) {
		System.out.println("Inside Cart service");
		Optional<Cart> cart = this.repository.findCartByCustomerId(custId);
		if(cart.isPresent())
		{
			System.out.println("Cart present "+cart.get().getCustomerID());
			return cart.get();
		}
		else
		{
			throw new CartNotFoundException("No Cart found for CustomerId : "+custId);
		}
	}

	@Override
	public Cart updateCartForCustomerId(long custId, Cart updatedcart) {
		if(updatedcart.getCustomerID() != custId)
		{
			throw new InvalidRequestException("CustomerId mismatch in request URI and body");
		}
		Optional<Cart> cart = this.repository.findCartByCustomerId(custId);
		if(cart.isPresent())
		{
			return this.repository.save(updatedcart);
		}
		else
		{
			throw new CartNotFoundException("No Cart found for CustomerId : "+custId);
		}
	}

	@Override
	public void deleteCartForCustomerId(long custId) {
		Optional<Cart> cart = this.repository.findCartByCustomerId(custId);
		if(cart.isPresent())
		{
			this.repository.deleteById(custId);
		}
		else
		{
			throw new CartNotFoundException("No Cart found for CustomerId : "+custId);
		}
		
	}

	@Override
	public Cart resetCartForCustomerId(long custId) {
		Optional<Cart> cart = this.repository.findCartByCustomerId(custId);
		if(cart.isPresent())
		{
			Cart resetCart = cart.get();
			resetCart.getShoppingCart().clear();
			return this.repository.save(resetCart);
		}
		else
		{
			throw new CartNotFoundException("No Cart found for CustomerId : "+custId);
		}
	}

	@Override
	public String addCartToOrderRequest(String jsonOrderRequest) {
		ObjectMapper om = new ObjectMapper();
		String cartAddedOrder = null;
		try {
			OrderRequest orderRequest = om.readValue(jsonOrderRequest, OrderRequest.class);
			long customerID = orderRequest.getCustomerID();
			Cart cart = this.getCartForCustomerId(customerID);
			orderRequest.setCart(cart);
			System.out.println(orderRequest.getCustomer().getCustomerAddress());
			cartAddedOrder = om.writeValueAsString(orderRequest);
			
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return cartAddedOrder;
	}

	@Override
	public void resetCartAfterSuccessfulOrder(String jsonOrder) {
		ObjectMapper om = new ObjectMapper();
		
		try {
			Order order = om.readValue(jsonOrder, Order.class);
			long customerID = order.getCustomerID();
			this.resetCartForCustomerId(customerID);
			
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		
	}

}
