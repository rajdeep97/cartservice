package com.meru.CartService.service;

import com.meru.CartService.model.Cart;

public interface CartService {

	public Cart createEmptyCartForCustomerId(long custId);

	public Cart getCartForCustomerId(long custId);

	public Cart updateCartForCustomerId(long custId, Cart updatedcart);

	public void deleteCartForCustomerId(long custId);

	public Cart resetCartForCustomerId(long custId);

	public String addCartToOrderRequest(String jsonOrderRequest);

	public void resetCartAfterSuccessfulOrder(String jsonOrder);

}
