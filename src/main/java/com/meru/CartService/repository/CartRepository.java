package com.meru.CartService.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.meru.CartService.model.Cart;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long>{

	@Query("SELECT c FROM Cart c WHERE c.customerID=?1")
	public Optional<Cart> findCartByCustomerId(long customerID);
}
