package com.meru.CartService.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.meru.CartService.model.Cart;
import com.meru.CartService.service.CartService;

@RestController
public class CartController {

	@Autowired
	private CartService service;
	
	@PostMapping("/cart/{customerId}")
	public ResponseEntity<Cart> createEmptyCart(@PathVariable long customerId)
	{
		Cart newCart = this.service.createEmptyCartForCustomerId(customerId);
		return new ResponseEntity<Cart>(newCart,HttpStatus.CREATED);
	}
	
	@GetMapping("/cart/{customerId}")
	public Cart getCartOfCustomer(@PathVariable Long customerId)
	{
		System.out.println("Inside Cart Controller "+ customerId);
		return this.service.getCartForCustomerId(customerId);	
	}
	
	@PutMapping("/cart/{customerId}")
	public Cart updateCartOfCustomer(@PathVariable long customerId, @RequestBody Cart updatedcart)
	{
		return this.service.updateCartForCustomerId(customerId,updatedcart);
	}
	
	@DeleteMapping("/cart/{customerId}")
	public void deleteCartOfCustomer(@PathVariable long customerId)
	{
		this.service.deleteCartForCustomerId(customerId);
	}
	
	@PostMapping("/cart/reset/{customerId}")
	public Cart resetCartOfCustomer(@PathVariable long customerId)
	{
		return this.service.resetCartForCustomerId(customerId);
	}
}
